import glob
import os
import subprocess

from mungy import date_group

files_826_cnt = glob.glob('split_by_hour/user_country_826_*.csv')
file_pairs_826_cnt = date_group.make_datetime_filename_pairs(files_826_cnt)

ds_train_4w_country826_cnt = date_group.make_training_testing_datasets(
    file_pairs_826_cnt,
    n_training_periods=4,
    training_period_type='week',
    n_testing_periods=1,
    testing_period_type='day',
    n_window_periods=2,
    window_period_type='week',
    min_testing_datasets=14
)

ds_train_4w_named_country826_cnt = date_group.name_training_testing_datasets(ds_train_4w_country826_cnt)

def mk_dir_output(slicing_name, country_id, dataset_index):
    return os.path.join(
        "data",
        "train_test_fnames",
        slicing_name,
        "country_{}".format(country_id),
        "{:03d}".format(dataset_index))


for i, ds in enumerate(ds_train_4w_named_country826_cnt):

    dir_output = mk_dir_output('train_4w_test_14d_by_2w', 826, i)

    os.makedirs(dir_output, exist_ok=True)

    fname_train = 'train__{}__{}.txt'.format(
        ds.train_dataset.interval.start.strftime('%Y-%m-%d_%H%M'),
        ds.train_dataset.interval.end.strftime('%Y-%m-%d_%H%M'))

    path_train = os.path.join(dir_output, fname_train)

    with open(path_train, "w") as f:
        print('writing train filenames to {}'.format(path_train))
        f.write("\n".join(sorted(ds.train_dataset.filepaths)))

    for j, test_ds in enumerate(ds.test_datasets[:14]):
        fname_test = 'test_{:03d}__{}__{}.txt'.format(
            j,
            ds.train_dataset.interval.start.strftime('%Y-%m-%d_%H%M'),
            ds.train_dataset.interval.end.strftime('%Y-%m-%d_%H%M'))

        path_test = os.path.join(dir_output, fname_test)

        with open(path_test, 'w') as f_test:
            print('writing test filenames to {}'.format(path_test))
            f_test.write('\n'.join(sorted(test_ds.filepaths)))
